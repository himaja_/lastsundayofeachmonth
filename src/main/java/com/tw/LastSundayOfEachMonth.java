package com.tw;

import java.time.LocalDate;

public class LastSundayOfEachMonth {

    public int[] getLastSundaysArray(int year) {
        int[] days = {31, isLeapYear(year) ? 29 : 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
        int[] lastSundays = new int[12];
        for (int month = 1; month <= 12; month++) {
            int day;
            for (day = days[month - 1]; getWeekDay(year, month, day) != 7; day--) ;
            lastSundays[month - 1] = day;
        }
        return lastSundays;
    }

    private boolean isLeapYear(int year) {
        if (year % 4 == 0) {
            if (year % 100 != 0)
                return true;
            else return year % 400 == 0;
        }
        return false;
    }

    private static int getWeekDay(int y, int m, int d) {
        LocalDate localDate = LocalDate.of(y, m, d);
        java.time.DayOfWeek dayOfWeek = localDate.getDayOfWeek();
        return dayOfWeek.getValue();
    }

}

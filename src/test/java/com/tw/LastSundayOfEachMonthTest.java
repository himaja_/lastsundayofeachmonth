package com.tw;

import org.junit.jupiter.api.Test;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

public class LastSundayOfEachMonthTest {

    @Test
    void shouldReturnLastSundayOfEachMonthArrayWhenYearIsGiven() {
        LastSundayOfEachMonth obj = new LastSundayOfEachMonth();

        int[] actualArray = obj.getLastSundaysArray(2022);
        int[] expectedArray = {30, 27, 27, 24, 29, 26, 31, 28, 25, 30, 27, 25};

        assertThat(actualArray, is(equalTo(expectedArray)));
    }
}
